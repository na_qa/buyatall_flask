from flask import Flask, redirect, url_for, request
# from flask_mysqldb import MySQL
import mysql.connector

app = Flask(__name__)

# app.config['MYSQL_HOST'] = 'test-mysql-master.caulyglobal.com'
# app.config['MYSQL_USER'] = 'vegas'
# app.config['MYSQL_PASSWORD'] = 'rktm0917)(!&'
# app.config['MYSQL_DB'] = 'buyatall'
# app.config['MYSQL_PORT'] = 3306
# app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
#
# mysql = MySQL(app)
table_name = 'buyatall_postbackinfo'

def updateAdkey(adkey, adid):
    con = mysql.connector.connect(host='test-mysql-master.caulyglobal.com', port='3306', database='buyatall', user='vegas',
                                        password='rktm0917)(!&')
    cursor = con.cursor(dictionary=True)
    sql = f'UPDATE {table_name} SET adkey=%s WHERE adid=%s'
    cursor.execute(sql, (adkey, adid, ))
    con.commit()
    con.close()

def getLandingUrl(adid):
    target = 'landing_url'
    con = mysql.connector.connect(host='test-mysql-master.caulyglobal.com', port='3306', database='buyatall',
                                  user='vegas',
                                  password='rktm0917)(!&')
    cursor = con.cursor(dictionary=True)
    sql = f'SELECT * FROM {table_name} WHERE adid=%s'
    cursor.execute(sql, (adid, ))
    obj = cursor.fetchone()
    cursor.close()
    con.close()
    return obj[target]

cache_adkey = {}
cache_landing_url = {}

import threading
def set_interval(interval):
    def decorator(function):
        def wrapper(*args, **kwargs):
            stopped = threading.Event()
            def loop(): # executed in another thread
                while not stopped.wait(interval): # until stopped
                    function(*args, **kwargs)
            t = threading.Thread(target=loop)
            t.daemon = True # stop if the program exits
            t.start()
            return stopped
        return wrapper
    return decorator

interval = 60
@set_interval(interval=interval)
def update_cache():
    for adid in cache_adkey:
        updateAdkey(adid=adid, adkey=cache_adkey[adid])
        landing_url = getLandingUrl(adid)
        cache_landing_url[adid] = landing_url
    print('update')
update_cache()

@app.route('/hoteltime')
def hello_world():
    adid = request.args['adid']
    adkey = request.args['adkey']

    # get objects
    # objects = PostBack.objects.get(adid=adid)
    # landing url
    if adid not in cache_adkey:
        cache_landing_url[adid] = getLandingUrl(adid)
    cache_adkey[adid] = adkey
    landing_url = cache_landing_url[adid]
    # update adkey
    # objects.adkey = adkey
    # objects.save()

    # with atomic
    # with transaction.atomic():
    #     objects = PostBack.objects.select_for_update().get(adid=adid)
    #     # landing url
    #     landing_url = objects.landing_url
    #     # update adkey
    #     objects.adkey = adkey
    #     objects.save()

    return redirect(landing_url)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False)
